Nội dung trong Day03
1. Mẫu Factory Method
- Case study (Partly)
  Ứng dụng quản lý danh sách người dùng, đa nền (Windows, Ubuntu, macOS).
  Solution: xem thêm ở file starUML.

  Kỹ thuật:
  * Sử dụng command-line cho các tác vụ tạo mới/sửa/xóa/list danh sách người dùng.
  Ví dụ: 
        Windows: https://pureinfotech.com/create-local-account-windows-10/
        Ubuntu: https://linuxize.com/post/how-to-create-users-in-linux-using-the-useradd-command/, ...

  * Kỹ thuật gọi thực thi chương trình console + bắt kết quả thực thi trong Java/.NET
  Java: 
        Search với từ khóa How to execute and catch result of command-line program, 
        https://commons.apache.org/proper/commons-exec/tutorial.html, 
        ...
  .NET: tương tự
        https://stackoverflow.com/questions/206323/how-to-execute-command-line-in-c-get-std-out-results
        ...


2. Mẫu Singleton
- Lý thuyết
- Ví dụ minh họa: .NET Solution có trong repo này.
- Các ứng dụng
    + create on demand / Lazy loading
    + Caching
        + Single object
        + Multiple objects (List/Dictionary/Hashtable)
        + Valid Time/TTL
        + Giới hạn kích thước bộ nhớ cache
        + Xem thêm các kỹ thuật storing cache items: Redis (https://redis.io/), Memcached (https://memcached.org/), ... Tất cả các kỹ thuật này đều có thể áp dụng được cho Java, .NET, PHP, ...
